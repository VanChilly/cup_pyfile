import matplotlib.pyplot as plt

import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms

import numpy as np
import random
import os
import time

os.environ["CUDA_VISIBLE_DEVICES"] = '0'


from src.utils import plot_tsne, fancy_dendrogram, save_obj, load_obj
from src.model import ANN, load_model
from src.prune_model import prune_model
from src.cluster_model import cluster_model
from src.train_test import train,test,adjust_learning_rate


# Training settings
parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
parser.add_argument('--batch_size', type=int, default=128, metavar='N',
                    help='input batch size for training (default: 256)')
parser.add_argument('--test_batch_size', type=int, default=256, metavar='N',
                    help='input batch size for testing (default: 256)')
parser.add_argument('--epochs', type=int, default=30, metavar='N',
                    help='number of epochs to train (default: 1)')
parser.add_argument('--lr', type=float, default=0.1, metavar='LR',
                    help='learning rate (default: 0.1)')
parser.add_argument('--momentum', type=float, default=0.9, metavar='M',
                    help='SGD momentum (default: 0.9)')
parser.add_argument('--weight_decay', type=float, default=1e-4, metavar='LR',
                    help='learning rate (default: 0.0001)')
parser.add_argument('--no_cuda', action='store_true', default=False,
                    help='disables CUDA training')
parser.add_argument('--seed', type=int, default=12346, metavar='S',
                    help='random seed (default: 12346)')
parser.add_argument('--log_interval', type=int, default=100, metavar='N',
                    help='how many batches to wait before logging training status')
parser.add_argument('--checkpoint_path', type=str, default='./checkpoints/ann.pth', 
                    metavar='S',help='path to store model training checkpoints')
parser.add_argument('--baseline', action='store_true', default=False, 
                    help='train baseline')
parser.add_argument('--prune_mode', type=str, choices=['random', 'l1', 'l2', 'cup'], 
                    help='prune method')
args = parser.parse_args()


def set_random_seed(seed):
        random.seed(seed)
        torch.manual_seed(seed)
        torch.cuda.manual_seed_all(seed)
        np.random.seed(args.seed)
        os.environ['PYTHONHASHSEED'] = str(seed)
        torch.backends.cudnn.deterministic = True


def main():
    #set device to CPU or GPU
    use_cuda = not args.no_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    #set all seeds for reproducibility

    set_random_seed(args.seed)
    dataset_path = './dataset/mnist'
    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}
    train_loader = torch.utils.data.DataLoader(
        datasets.MNIST(dataset_path, train=True, download=True,
                    transform=transforms.Compose([
                        transforms.ToTensor()#,
                        #transforms.Normalize((0.1307,), (0.3081,))
                    ])),
        batch_size=args.batch_size, shuffle=True, **kwargs)
    test_loader = torch.utils.data.DataLoader(
        datasets.MNIST(dataset_path, train=False, transform=transforms.Compose([
                        transforms.ToTensor()#,
                        #transforms.Normalize((0.1307,), (0.3081,))
                    ])),
        batch_size=args.test_batch_size, shuffle=True, **kwargs)

    # train baseline
    if args.baseline:
        train_baseline(device, train_loader, test_loader)
    else:
        prune(device, train_loader, test_loader)


def train_baseline(device, train_loader, test_loader):

    ann = ANN().to(device)
            
    optimizer = optim.SGD(ann.parameters(),
                          lr=args.lr,
                          momentum=args.momentum,
                          weight_decay=args.weight_decay,
                          nesterov=False)

    if not os.path.isfile(args.checkpoint_path):
        for epoch in range(1, args.epochs + 1):
            adjust_learning_rate(args, optimizer, epoch)
            train(args, ann, device, train_loader, optimizer, epoch)
            test_loss,test_accuracy = test(args, ann, device, test_loader)

            torch.save({
                        'epoch': epoch,
                        'model_state_dict': ann.state_dict(),
                        'optimizer_state_dict': optimizer.state_dict(),
                        'loss': test_loss,
                    }, args.checkpoint_path, pickle_protocol=4)


def prune(device, train_loader, test_loader):
    prune_mode = args.prune_mode
    if prune_mode == 'random':
        suffix = '_small_random.pth'
    elif prune_mode == 'l2':
        suffix = '_small_l2.pth'
    elif prune_mode == 'l1':
        suffix = '_small_l1.pth'
    elif prune_mode == 'cup':
        suffix = '_small_cup.pth'
    else:
        raise NotImplementedError(prune_mode)

    ann, optimizer = load_model('ann', 'sgd', args)
    # to show baseline acc and loss
    test(args, ann, device, test_loader)

    # pruning config
    if prune_mode == 'cup':
        cluster_args = {
            'cluster_layers': {1:400, 3:240},
            'conv_feature_size': 1,
            'reshape_exists': False,
            'features': 'both',
            'channel_reduction': 'fro',
            'use_bias': False,
            'linkage_method': 'ward',
            'distance_metric': 'euclidean',
            'cluster_criterion': 'hierarchical_trunc',
            'distance_threshold': 1.60,
            'merge_criterion': 'max_l2_norm',
            'verbose': False
        }
        model_modifier = cluster_model(ann, cluster_args)
        pruned_ann = model_modifier.cluster_model()
    else:
        pruning_args = {
            'criterion': prune_mode,
            'use_bias': True,
            'prune_layers': {1: 400, 3: 240},
            'conv_feature_size': 4
        }
        model_modifier = prune_model(ann, pruning_args)
        pruned_ann = model_modifier.prune_model(prune_mode)

    path = args.checkpoint_path[:-4] + suffix 
    pruned_ann.to(device)

    val_loss_no_retrain, val_accuracy_no_retrain = test(args, pruned_ann, device, test_loader, verbose=False)
    optimizer = optim.SGD(pruned_ann.parameters(), lr=args.lr, momentum=args.momentum, weight_decay=args.weight_decay, nesterov=False)

    best_val_accuracy_retrain = 0

    if not os.path.isfile(path):    
        for epoch in range(1, args.epochs+1):
            adjust_learning_rate(args, optimizer, epoch)
            train(args, pruned_ann, device, train_loader, optimizer, epoch)
            val_loss_retrain, val_accuracy_retrain = test(args, pruned_ann, device, test_loader)

            if val_accuracy_retrain > best_val_accuracy_retrain:  
                torch.save(pruned_ann, path, pickle_protocol=4)            
                best_val_accuracy_retrain = val_accuracy_retrain   
    else:
        pruned_ann = torch.load(path)
        val_loss_retrain, val_accuracy_retrain = test(args, pruned_ann, device, test_loader, verbose=False)
        best_val_accuracy_retrain = val_accuracy_retrain
            
    print('Accuracy post pruning : {} (without retraining), {} (with retraining)'.format(val_accuracy_no_retrain, best_val_accuracy_retrain))  
    

if __name__ == '__main__':
    main()

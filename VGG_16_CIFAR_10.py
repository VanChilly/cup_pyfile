import argparse
import torch
import torch.nn as nn
import torch.nn.functional as F
import torch.optim as optim
from torchvision import datasets, transforms
from tensorboardX import SummaryWriter

import numpy as np
import random
import os
import time
import copy

os.environ["CUDA_VISIBLE_DEVICES"] = '0'

from src.utils import plot_tsne, fancy_dendrogram, save_obj, load_obj
from src.model import VGG, load_model
from src.prune_model import prune_model
from src.cluster_model import cluster_model
from src.train_test import train, test, adjust_learning_rate_nips, adjust_learning_rate_iccv
from src.compute_flops import print_model_param_nums,print_model_param_flops


parser = argparse.ArgumentParser(description='PyTorch MNIST Example')
parser.add_argument('--batch-size', type=int, default=64, metavar='N',
                    help='input batch size for training (default: 128)')
parser.add_argument('-j', '--workers', default=4, type=int, metavar='N',
                    help='number of data loading workers (default: 4)')
parser.add_argument('--epochs', default=160, type=int, metavar='N',
                    help='number of total epochs to run')
parser.add_argument('--lr', '--learning-rate', default=0.1, type=float,
                    metavar='LR', help='initial learning rate')
parser.add_argument('--momentum', default=0.9, type=float, metavar='M',
                    help='momentum')
parser.add_argument('--weight-decay', '--wd', default=1e-4, type=float,
                    metavar='W', help='weight decay (default: 1e-4)')
parser.add_argument('--no-cuda', action='store_true', default=False,
                    help='disables CUDA training')
parser.add_argument('--seed', type=int, default=12346, metavar='S',
                    help='random seed (default: 12346)')
parser.add_argument('--num_output', type=int, default=10, metavar='S',
                    help='number of classes(default: 10)')
parser.add_argument('--log-interval', type=int, default=100, metavar='N',
                    help='how many batches to wait before logging training status')
parser.add_argument('--checkpoint_path', type=str, default='./checkpoints/vgg16_cifar10.pth',
                    metavar='S', help='path to store model training checkpoints')
parser.add_argument('--gpu', type=int, default=[5], nargs='+',
                    help='used gpu')
parser.add_argument('--baseline', action='store_true', default=False,
                    help='choose to train baseline')
parser.add_argument('--cup_mode', type=str, choices=['cup', 'cup-ss'],
                    help='use cup of cup-ss(single-shot)')
parser.add_argument('--T', type=float, default=0.9,
                    help='CUP distance_threshold')
parser.add_argument('--slop', type=float, default=0.02,
                    help='CUP-SS slop')

args = parser.parse_args()


# set all seeds for reproducibility
def set_random_seed(seed):
    random.seed(seed)
    torch.manual_seed(seed)
    torch.cuda.manual_seed_all(seed)
    np.random.seed(args.seed)
    os.environ['PYTHONHASHSEED'] = str(seed)
    torch.backends.cudnn.deterministic = True


def main():
    use_cuda = not args.no_cuda and torch.cuda.is_available()
    device = torch.device("cuda" if use_cuda else "cpu")

    writer = SummaryWriter('log/vgg_16_cifar10/base_iccv_param/')
    set_random_seed(args.seed)

    kwargs = {'num_workers': 1, 'pin_memory': True} if use_cuda else {}

    normalize = transforms.Normalize(mean=[0.485, 0.456, 0.406],
                                     std=[0.229, 0.224, 0.225])

    train_loader = torch.utils.data.DataLoader(
        datasets.CIFAR10(root='data/', train=True, transform=transforms.Compose([
            transforms.RandomHorizontalFlip(),
            transforms.RandomCrop(32, 4),
            transforms.ToTensor(),
            normalize,
        ]), download=True),
        batch_size=args.batch_size, shuffle=True,
        num_workers=args.workers, pin_memory=True)

    val_loader = torch.utils.data.DataLoader(
        datasets.CIFAR10(root='data/', train=False, transform=transforms.Compose([
            transforms.ToTensor(),
            normalize,
        ])),
        batch_size=args.batch_size, shuffle=False,
        num_workers=args.workers, pin_memory=True)

    vgg16_bn = VGG('VGG16', num_output=args.num_output)
    vgg16_bn = vgg16_bn.to(device)

    optimizer = optim.SGD(vgg16_bn.parameters(), lr=args.lr, momentum=args.momentum, weight_decay=args.weight_decay,
                          nesterov=False)
    # begin
    if args.baseline:
        train_vgg_baseline(device, optimizer, vgg16_bn, train_loader, val_loader, writer)
    elif args.cup_mode == 'cup':
        prune_w_cup(device, train_loader, val_loader, writer)
    elif args.cup_mode == 'cup-ss':
        prune_w_cup_ss(device, train_loader, val_loader, writer)
    else:
        raise NotImplementedError('Unknown mode: ', args.cup_mode)


def train_vgg_baseline(device, optimizer, vgg16_bn, train_loader, val_loader, writer):
    test_loss = 0
    best_val_acc = 0

    if not os.path.isfile(args.checkpoint_path):
        for epoch in range(1, args.epochs + 1):
            start = time.time()
            args.lr = adjust_learning_rate_iccv(args, optimizer, epoch)
            train_loss, train_acc = train(args, vgg16_bn, device, train_loader, optimizer, epoch)
            val_loss, val_acc = test(args, vgg16_bn, device, val_loader)

            writer.add_scalars('base_model/loss', {'train_loss': train_loss,
                                                   'val_loss': val_loss}, epoch)
            writer.add_scalars('base_model/accuracy', {'train_acc': train_acc,
                                                       'val_acc': val_acc}, epoch)

            print('Time taken for epoch : {}\n'.format(time.time() - start))

            if val_acc > best_val_acc:
                best_val_acc = val_acc

                torch.save({
                    'epoch': epoch,
                    'model_state_dict': vgg16_bn.state_dict(),
                    'optimizer_state_dict': optimizer.state_dict(),
                    'loss': val_loss,
                }, args.checkpoint_path, pickle_protocol=4)
    else:
        vgg16_bn, optimizer = load_model('vgg16_bn', 'sgd', args)
        orig_loss, orig_acc = test(args, vgg16_bn, device, val_loader, verbose=False)
        best_val_acc = orig_acc

    print('original model accuracy is {}'.format(best_val_acc))


def prune_w_cup(device, train_loader, val_loader, writer):
    set_random_seed(args.seed)
    vgg16_bn, optimizer = load_model('vgg16_bn', 'sgd', args)
    orig_loss, orig_acc = test(args, vgg16_bn, device, val_loader)

    T = args.T
    if T == 0.9:
        suffix = '_small_cup_t_point_9.pth'
    elif T == 0.95:
        suffix = '_small_cup_t_point_95.pth'
    else:
        raise NotImplementedError('T:', T)
    cluster_args = {
        'cluster_layers': {0: 0, 3: 0, 7: 0, 10: 0, 14: 0, 17: 0, 20: 0, 24: 0, 27: 0, 30: 0, 34: 0, 37: 0, 40: 0},
        'conv_feature_size': 1,
        'reshape_exists': True,
        'features': 'both',
        'channel_reduction': 'fro',
        'use_bias': True,
        'linkage_method': 'ward',
        'distance_metric': 'euclidean',
        'cluster_criterion': 'hierarchical',
        'distance_threshold': 0.90,
        'merge_criterion': 'max_l2_norm',
        'verbose': False
    }

    path = args.checkpoint_path[:-4] + suffix
    model_modifier = cluster_model(vgg16_bn, cluster_args)
    vgg16_bn_clustered = model_modifier.cluster_model()  # [int(nodes*drop_percentage) for nodes in [500,300]])
    vgg16_bn_clustered.cuda()

    val_loss_no_retrain, val_accuracy_no_retrain = test(args, vgg16_bn_clustered, device, val_loader)

    args.epochs = 160
    args.lr = 0.1

    optimizer = optim.SGD(vgg16_bn_clustered.parameters(), lr=args.lr, momentum=args.momentum,
                          weight_decay=args.weight_decay, nesterov=False)

    best_val_acc = 0

    if not os.path.isfile(path):
        for epoch in range(1, args.epochs + 1):
            args.lr = adjust_learning_rate_iccv(args, optimizer, epoch)
            start = time.time()
            train_loss, train_acc = train(args, vgg16_bn_clustered, device, train_loader, optimizer, epoch)
            val_loss, val_acc = test(args, vgg16_bn_clustered, device, val_loader)

            if val_acc > best_val_acc:
                torch.save(vgg16_bn_clustered, path, pickle_protocol=4)
                best_val_acc = val_acc

            writer.add_scalars('clustered/0_point_90_loss', {'train_loss': train_loss,
                                                             'val_loss': val_loss}, epoch)
            writer.add_scalars('clustered/0_point_90_accuracy', {'train_acc': train_acc,
                                                                 'val_acc': val_acc}, epoch)
            print('Time taken for epoch : {}\n'.format(time.time() - start))
    else:
        vgg16_bn_clustered = torch.load(path)
        val_loss, val_acc = test(args, vgg16_bn_clustered, device, val_loader, verbose=False)
        best_val_acc = val_acc

    print('Original accuracy {}, compressed model accuracy {}, accuracy drop {}'.format(orig_acc, best_val_acc,
                                                                                        orig_acc - best_val_acc))


def cal_params_flops(vgg16_bn, vgg16_bn_clustered):
    print('--- stats for original model ---')
    print_model_param_nums(vgg16_bn)
    print_model_param_flops(vgg16_bn.cpu(), input_res=32)

    print('--- stats for compressed model ---')
    print_model_param_nums(vgg16_bn_clustered)
    print_model_param_flops(vgg16_bn_clustered.cpu(), input_res=32)


def cal_exec_time(cluster_args, vgg16_bn, val_loader):
    device = torch.device("cpu")

    T_values = [0]

    for T in T_values:

        cluster_args['distance_threshold'] = T

        model_modifier = cluster_model(vgg16_bn, cluster_args)
        vgg16_bn_clustered = model_modifier.cluster_model()
        if device == torch.device("cuda"):
            vgg16_bn_clustered.cuda()

        args.epochs = 1
        args.lr = 0.1

        optimizer = optim.SGD(vgg16_bn_clustered.parameters(), lr=args.lr, momentum=args.momentum,
                              weight_decay=args.weight_decay, nesterov=False)

        start = time.time()
        val_loss, val_acc = test(args, vgg16_bn_clustered, device, val_loader)

        print('Time taken for epoch : {}\n'.format(time.time() - start))

        print_model_param_nums(vgg16_bn)
        print_model_param_nums(vgg16_bn_clustered)

        print_model_param_flops(vgg16_bn.cpu(), input_res=32)
        print_model_param_flops(vgg16_bn_clustered.cpu(), input_res=32)


def prune_w_cup_ss(device, train_loader, val_loader, writer):
    set_random_seed(args.seed)
    vgg16_bn_clustered = VGG('VGG16', num_output=args.num_output)
    vgg16_bn_clustered = vgg16_bn_clustered.to(device)

    cluster_args = {
        'cluster_layers': {0: 0, 3: 0, 7: 0, 10: 0, 14: 0, 17: 0, 20: 0, 24: 0, 27: 0, 30: 0, 34: 0, 37: 0, 40: 0},
        'conv_feature_size': 1,
        'reshape_exists': True,
        'features': 'both',
        'channel_reduction': 'fro',
        'use_bias': True,
        'linkage_method': 'ward',
        'distance_metric': 'euclidean',
        'cluster_criterion': 'hierarchical',
        'distance_threshold': 0.95,
        'merge_criterion': 'max_l2_norm',
        'verbose': False
    }

    path = args.checkpoint_path[:-4] + '_cupSS_K_point02.pth'

    T_values = {}
    slope = 0.02

    for epoch in range(args.epochs + 1):
        T_values[epoch] = slope * (epoch - 1)

    args.epochs = 160
    args.lr = 0.1
    best_val_acc = 0
    flag = True

    if not os.path.isfile(path):
        for epoch in range(1, args.epochs + 1):
            if epoch in T_values.keys() and flag:
                print('changing T value to {}'.format(T_values[epoch]))
                cluster_args['distance_threshold'] = T_values[epoch]
                model_modifier = cluster_model(vgg16_bn_clustered, cluster_args)
                vgg16_bn_clustered = model_modifier.cluster_model()
                vgg16_bn_clustered.cuda()
                optimizer = optim.SGD(vgg16_bn_clustered.parameters(), lr=args.lr, momentum=args.momentum,
                                      weight_decay=args.weight_decay, nesterov=False)

                params = print_model_param_nums(copy.deepcopy(vgg16_bn_clustered))
                flops = print_model_param_flops(copy.deepcopy(vgg16_bn_clustered).cpu(), input_res=32)

                if (params / 1e6 <= 0.9):
                    print('stop filter pruning')
                    flag = False

            args.lr = adjust_learning_rate_iccv(args, optimizer, epoch)
            start = time.time()
            train_loss, train_acc = train(args, vgg16_bn_clustered, device, train_loader, optimizer, epoch)
            val_loss, val_acc = test(args, vgg16_bn_clustered, device, val_loader)

            if val_acc > best_val_acc:
                torch.save(vgg16_bn_clustered, path, pickle_protocol=4)
                best_val_acc = val_acc

            writer.add_scalars('vgg16_cifar10_small_cupSS_K_point2/loss', {'train_loss': train_loss,
                                                                           'val_loss': val_loss}, epoch)
            writer.add_scalars('vgg16_cifar10_small_cupSS_K_point2/accuracy', {'train_acc': train_acc,
                                                                               'val_acc': val_acc}, epoch)
            writer.add_scalars('vgg16_cifar10_small_cupSS_K_point2/params', {'params': params}, epoch)
            writer.add_scalars('vgg16_cifar10_small_cupSS_K_point2/flops', {'flops': flops}, epoch)
            print('Time taken for epoch : {}\n'.format(time.time() - start))
    else:
        vgg16_bn_clustered = torch.load(path)
        val_loss, val_acc = test(args, vgg16_bn_clustered, device, val_loader, verbose=False)
        best_val_acc = val_acc

    print('Best accuracy {}'.format(best_val_acc))
# !/bin/bash
# train baseline
# python -u MLP_MNIST.py --baseline --no_cuda > log_baseline.log

# prune the baseline w/ random
# python -u MLP_MNIST.py --no_cuda --prune_mode random > log/log_random.log

# prune the baseline w/ l1
# python -u MLP_MNIST.py --no_cuda --prune_mode l1 > log/log_l1.log

# prune the baseline w/ l2
# python -u MLP_MNIST.py --no_cuda --prune_mode l2 > log/log_l2.log

# prune the baseline w/ CUP
# python -u MLP_MNIST.py \
#    --no_cuda --lr 0.1 --epochs 30 --prune_mode cup > log/log_cup.log

# test
python MLP_MNIST.py --no_cuda --prune_mode l1